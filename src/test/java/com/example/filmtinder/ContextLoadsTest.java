package com.example.filmtinder;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;

@ComponentScan("com.example.filmtinder")
@AutoConfigureDataJpa
@Slf4j
@SpringBootTest
@Profile("cloud-db")
public class ContextLoadsTest {
    @Autowired
    ApplicationContext context;

    @Nested
    public class RuslanContextLoadsTest {

        @Test
        void contextLoads() {
            log.info("beans = [{}]", context.getBeanDefinitionNames());
        }

    }

}
