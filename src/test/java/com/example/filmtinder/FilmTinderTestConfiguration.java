package com.example.filmtinder;

import org.springframework.boot.test.context.TestConfiguration;

// N.B. This is for local development
@TestConfiguration(proxyBeanMethods = false)
public class FilmTinderTestConfiguration {

}
