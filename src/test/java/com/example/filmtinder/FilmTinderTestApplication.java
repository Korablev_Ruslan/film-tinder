package com.example.filmtinder;

import org.springframework.boot.SpringApplication;

public class FilmTinderTestApplication {

    public static void main(String[] args) {
        SpringApplication.from(FilmTinderApplication::main)
                .with(FilmTinderTestConfiguration.class)
                .run(args);
    }
}
