package com.example.filmtinder.module.utils;

import com.example.filmtinder.parser.ParserService;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Slf4j
public class TestUtils {

    public static <E> E getRandomElement(List<E> list) {
        return list.stream().findAny().get();
    }

    public static void parseFilmsAndGenres(ParserService parserService) {
        try {
            log.info("Start parsing films & genres");
            parserService.parseMePasha();
            Thread.sleep(TimeUnit.SECONDS.toMillis(45));
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } finally {
            parserService.stopParsing();
            log.info("End parsing films & genres");
        }
    }

}
