package com.example.filmtinder.module.utils;

import com.example.filmtinder.unit.lobby.LobbyEntity;
import com.example.filmtinder.unit.lobby.LobbyRepository;
import com.example.filmtinder.unit.lobby.LobbyStatus;
import com.example.filmtinder.utils.FilmsChoosingTimeoutRunnable;
import com.example.filmtinder.utils.GenresChoosingTimeoutRunnable;
import com.example.filmtinder.utils.SessionStartTimeoutRunnable;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;


class RunnableUtilsTest {

    @Test
    void testSessionStartTimeoutRunnableExecutingWithException() {
        LobbyEntity lobbyEntity = Mockito.mock(LobbyEntity.class);
        LobbyRepository lobbyRepository = Mockito.mock(LobbyRepository.class);
        assertThatCode(() -> new SessionStartTimeoutRunnable(lobbyEntity, lobbyRepository).run())
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testSessionStartTimeoutRunnableNormalExecuting() {
        LobbyEntity lobbyEntity = Mockito.mock(LobbyEntity.class);
        LobbyRepository lobbyRepository = Mockito.mock(LobbyRepository.class);
        lenient()
                .doReturn(
                        Optional.of(
                                LobbyEntity.builder()
                                        .lobbyId("mockLobbyId")
                                        .finishedCount(2)
                                        .joinedPersons(Set.of("10", "2", "3"))
                                        .authorId("10")
                                        .chosenGenres(Set.of())
                                        .status(LobbyStatus.READY_TO_START)
                                        .build()
                        )).when(lobbyRepository).findById(any());

        assertThatCode(() -> new SessionStartTimeoutRunnable(lobbyEntity, lobbyRepository).run())
                .doesNotThrowAnyException();
    }

    @Test
    void testGenresChoosingTimeoutRunnableExecutingWithException() {
        LobbyEntity lobbyEntity = Mockito.mock(LobbyEntity.class);
        LobbyRepository lobbyRepository = Mockito.mock(LobbyRepository.class);
        assertThatCode(() -> new GenresChoosingTimeoutRunnable(lobbyEntity, lobbyRepository).run())
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testGenresChoosingTimeoutRunnableNormalExecuting() {
        LobbyEntity lobbyEntity = Mockito.mock(LobbyEntity.class);
        LobbyRepository lobbyRepository = Mockito.mock(LobbyRepository.class);
        lenient()
                .doReturn(
                        Optional.of(
                                LobbyEntity.builder()
                                        .lobbyId("mockLobbyId")
                                        .finishedCount(2)
                                        .joinedPersons(Set.of("10", "2", "3"))
                                        .authorId("10")
                                        .chosenGenres(Set.of())
                                        .status(LobbyStatus.GENRES_CHOOSING)
                                        .build()
                        )).when(lobbyRepository).findById(any());
        assertThatCode(() -> new GenresChoosingTimeoutRunnable(lobbyEntity, lobbyRepository).run())
                .doesNotThrowAnyException();
    }

    @Test
    void testFilmsChoosingTimeoutRunnableExecutingWithException() {
        LobbyEntity lobbyEntity = Mockito.mock(LobbyEntity.class);
        LobbyRepository lobbyRepository = Mockito.mock(LobbyRepository.class);
        assertThatCode(() -> new FilmsChoosingTimeoutRunnable(lobbyEntity, lobbyRepository).run())
                .isInstanceOf(RuntimeException.class);

        Mockito.verify(lobbyEntity, Mockito.times(1)).getLobbyId();
    }

    @Test
    void testFilmsChoosingTimeoutRunnableNormalExecuting() {
        LobbyEntity lobbyEntity = Mockito.mock(LobbyEntity.class);
        LobbyRepository lobbyRepository = Mockito.mock(LobbyRepository.class);
        lenient()
                .doReturn(
                        Optional.of(
                                LobbyEntity.builder()
                                        .lobbyId("mockLobbyId")
                                        .finishedCount(2)
                                        .joinedPersons(Set.of("10", "2", "3"))
                                        .authorId("10")
                                        .chosenGenres(Set.of())
                                        .status(LobbyStatus.FILMS_CHOOSING)
                                        .build()
                        )).when(lobbyRepository).findById(any());

        assertThatCode(() -> new FilmsChoosingTimeoutRunnable(lobbyEntity, lobbyRepository).run())
                .doesNotThrowAnyException();

        Mockito.verify(lobbyEntity, Mockito.times(1)).getLobbyId();
    }
}