package com.example.filmtinder.module.utils;

import com.example.filmtinder.db.genre.GenreRepository;
import com.example.filmtinder.parser.ParserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FilmParser {

    @Autowired
    private ParserService parserService;
    @Autowired
    private GenreRepository genreRepository;

    public void parseFilms() throws InterruptedException {
        if (genreRepository.findAll().isEmpty()) {
            parserService.parseMePasha();
            Thread.sleep(15_000);
            parserService.stopParsing();
            if (genreRepository.findAll().isEmpty()) {
                throw new RuntimeException("No genres!");
            }
        }
    }
}
