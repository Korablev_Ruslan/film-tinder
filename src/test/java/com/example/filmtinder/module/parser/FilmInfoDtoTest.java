package com.example.filmtinder.module.parser;

import com.example.filmtinder.db.film.FilmEntity;
import com.example.filmtinder.parser.FilmInfoDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FilmInfoDtoTest {

    public static final String NAME = "NAME";
    public static final String DESCIPTION = "Desciption";
    public static final String HTTP = "http";

    @Test
    void convertFIlmToDto() {
        FilmEntity film = new FilmEntity();
        film.setId(10L);
        film.setName(NAME);
        film.setDuration(10);
        film.setDescription(DESCIPTION);
        film.setRating_imdb(10F);
        film.setRating_kp(123F);
        film.setPoster_url(HTTP);
        film.setRelease_year(2002);

        FilmInfoDto actual = FilmInfoDto.convert(film);

        assertEquals(10, actual.getId());
        assertEquals(NAME, actual.getName());
        assertEquals(10, actual.getDuration());
        assertEquals(DESCIPTION, actual.getDescription());
        assertEquals(10F, actual.getRating_imdb());
        assertEquals(123F, actual.getRating_kp());
        assertEquals(HTTP, actual.getPoster_url());
        assertEquals(2002, actual.getRelease_year());

    }
}
