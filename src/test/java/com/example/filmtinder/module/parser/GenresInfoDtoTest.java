package com.example.filmtinder.module.parser;

import com.example.filmtinder.db.genre.GenreEntity;
import com.example.filmtinder.parser.GenresInfoDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

public class GenresInfoDtoTest {

    public static final String NAME = "NAme";
    public static final long ID = 10L;

    @Nested
    public class ArtyomGenresInfoDtoTests {

        @Test
        void validConvertFilm() {
            GenreEntity genre = new GenreEntity();
            genre.setId(ID);
            genre.setName(NAME);
            genre.setFilms(List.of());

            GenresInfoDto actual = GenresInfoDto.convert(genre);

            Assertions.assertEquals(10L, actual.getId());
            Assertions.assertEquals(NAME, actual.getName());
        }
    }
}
