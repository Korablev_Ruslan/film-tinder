package com.example.filmtinder.module.parser;

import com.example.filmtinder.db.film.FilmDto;
import com.example.filmtinder.db.genre.GenreDto;
import com.example.filmtinder.db.genre.GenreEntity;
import com.example.filmtinder.openapi.kinopoisk.model.*;
import com.example.filmtinder.parser.ParserService;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class ParserServiceTest {

    private static final String GENRE_NAME_1 = "Драма";
    private static final String GENRE_NAME_2 = "Комедия";
    private static final String GENRE_SLUG_1 = "drama";
    private static final String GENRE_SLUG_2 = "comedy";
    public static final String FILM_NAME_1 = "Фильм 1";
    public static final String FILM_NAME_2 = "Фильм 2";
    public static final String MOVIE_TYPE = "movie";
    public static final int YEAR = 2020;
    public static final int YEAR_2 = 2022;
    public static final String PREVIEW_URL = "https://...";
    public static final long DURATION = 120L;
    public static final MovieDtoV13 MOVIE_1 = new MovieDtoV13().name(FILM_NAME_1).movieLength(BigDecimal.valueOf(DURATION)).genres(List.of(new ItemName().name(GENRE_NAME_1))).type(MOVIE_TYPE).id(BigDecimal.valueOf(123)).year(BigDecimal.valueOf(YEAR)).poster(new ShortImage().previewUrl(PREVIEW_URL)).rating(new Rating().kp(BigDecimal.TEN).imdb(BigDecimal.ONE));
    public static final MovieDtoV13 MOVIE_2 = new MovieDtoV13().name(FILM_NAME_2).movieLength(BigDecimal.valueOf(DURATION)).genres(List.of(new ItemName().name(GENRE_NAME_2))).type(MOVIE_TYPE).id(BigDecimal.valueOf(124)).year(BigDecimal.valueOf(YEAR_2)).poster(new ShortImage().previewUrl(PREVIEW_URL)).rating(new Rating().kp(BigDecimal.TEN).imdb(BigDecimal.ONE));
    public static final MovieDtoV13 MOVIE_3 = new MovieDtoV13().name(FILM_NAME_2).genres(List.of(new ItemName().name(GENRE_NAME_2))).type(MOVIE_TYPE).id(BigDecimal.valueOf(124)).year(BigDecimal.valueOf(YEAR_2)).poster(new ShortImage().previewUrl(PREVIEW_URL)).rating(new Rating().kp(BigDecimal.TEN).imdb(BigDecimal.ONE));
    public static final MovieDocsResponseDtoV13 MOVIE_DOCS_RESPONSE_1 = new MovieDocsResponseDtoV13().limit(BigDecimal.valueOf(100)).total(BigDecimal.valueOf(321)).page(BigDecimal.valueOf(2)).pages(BigDecimal.valueOf(10)).docs(List.of(MOVIE_1));
    public static final MovieDocsResponseDtoV13 MOVIE_DOCS_RESPONSE_2 = new MovieDocsResponseDtoV13().limit(BigDecimal.valueOf(100)).page(BigDecimal.valueOf(2)).pages(BigDecimal.valueOf(10)).docs(List.of(MOVIE_1, MOVIE_2));
    public static final LinkedMovie LINKED_MOVIE = new LinkedMovie().name(FILM_NAME_2).id(BigDecimal.ONE);
    public static final MovieDocsResponseDtoV13 MOVIE_DOCS_RESPONSE_3 = new MovieDocsResponseDtoV13().limit(BigDecimal.valueOf(100)).page(BigDecimal.valueOf(2)).pages(BigDecimal.valueOf(10)).docs(List.of(new MovieDtoV13().name(FILM_NAME_1).movieLength(BigDecimal.valueOf(DURATION)).genres(List.of(new ItemName().name(GENRE_NAME_1))).type(MOVIE_TYPE).id(BigDecimal.valueOf(123)).year(BigDecimal.valueOf(YEAR)).poster(new ShortImage().previewUrl(PREVIEW_URL)).sequelsAndPrequels(List.of(LINKED_MOVIE)).rating(new Rating().kp(BigDecimal.TEN).imdb(BigDecimal.ONE))));
    public static final MovieDocsResponseDtoV13 MOVIE_DOCS_RESPONSE_4 = new MovieDocsResponseDtoV13().limit(BigDecimal.valueOf(100)).page(BigDecimal.valueOf(2)).pages(BigDecimal.valueOf(10)).docs(List.of(MOVIE_3));
    public static final float RATING_KP = 10F;
    public static final float RATING_IMDB = 1F;

    @Nested
    public class ArtyomParserServiceTest {

        @Test
        void whenGetFailedRequestGetGenres_thenReturnNull() {
            ResponseEntity<PossibleValueDto[]> exchange = new ResponseEntity<>(HttpStatusCode.valueOf(403));

            List<GenreDto> genreDtos = ParserService.getGenreDtos(exchange);

            assertNull(genreDtos);
        }

        @Test
        void whenPassNullToParsingGenres_thenReturnNull() {
            ResponseEntity<PossibleValueDto[]> exchange = null;

            List<GenreDto> genreDtos = ParserService.getGenreDtos(exchange);

            assertNull(genreDtos);
        }

        @Test
        void passValidGenres_thenNoErrors() {
            PossibleValueDto genre1 = new PossibleValueDto().name(GENRE_NAME_1).slug(GENRE_SLUG_1);
            PossibleValueDto genre2 = new PossibleValueDto().name(GENRE_NAME_2).slug(GENRE_SLUG_2);
            ResponseEntity<PossibleValueDto[]> exchange = ResponseEntity.ok(new PossibleValueDto[]{genre1, genre2});

            List<GenreDto> genreDtos = ParserService.getGenreDtos(exchange);

            assertNotNull(genreDtos);
            assertEquals(2, genreDtos.size());
            assertEquals(GENRE_NAME_1, genreDtos.get(0).getName());
            assertEquals(GENRE_NAME_2, genreDtos.get(1).getName());
        }

        @Test
        void passInvalidOneGenre_thenNoErrorsButSkipOne() {
            PossibleValueDto genre1 = new PossibleValueDto().name(null).slug(GENRE_SLUG_1);
            PossibleValueDto genre2 = new PossibleValueDto().name(GENRE_NAME_2).slug(GENRE_SLUG_2);
            ResponseEntity<PossibleValueDto[]> exchange = ResponseEntity.ok(new PossibleValueDto[]{genre1, genre2});

            List<GenreDto> genreDtos = ParserService.getGenreDtos(exchange);

            assertNotNull(genreDtos);
            assertEquals(1, genreDtos.size());
            assertEquals(GENRE_NAME_2, genreDtos.get(0).getName());
        }

        @Test
        void passEmptyGenres_thenReturnEmpty() {
            ResponseEntity<PossibleValueDto[]> exchange = ResponseEntity.ok(new PossibleValueDto[]{});

            List<GenreDto> genreDtos = ParserService.getGenreDtos(exchange);

            assertNotNull(genreDtos);
            assertEquals(0, genreDtos.size());
        }

        @Test
        void whenCheckFailedMovieResponse_thenReturnEmpty() {
            ResponseEntity<MovieDocsResponseDtoV13> exchange = ResponseEntity.badRequest().build();

            MovieDocsResponseDtoV13 movieResponse = ParserService.checkMovieResponse(exchange);

            assertNull(movieResponse);
        }

        @Test
        void whenPassNullBodyToParsingFilms_thenReturnNull() {
            ResponseEntity<MovieDocsResponseDtoV13> exchange = ResponseEntity.ok(null);

            MovieDocsResponseDtoV13 movieResponse = ParserService.checkMovieResponse(exchange);

            assertNull(movieResponse);
        }

        @Test
        void whenPassValidBodyToParsingFilms_thenReturnValid() {
            ResponseEntity<MovieDocsResponseDtoV13> exchange = ResponseEntity.ok(MOVIE_DOCS_RESPONSE_1);

            MovieDocsResponseDtoV13 movieResponse = ParserService.checkMovieResponse(exchange);

            assertNotNull(movieResponse);
            assertEquals(MOVIE_DOCS_RESPONSE_1, movieResponse);
        }


        @Test
        void whenPassNullToGetMovieDto_thenReturnEmptyList() {
            MovieDocsResponseDtoV13 movieResponse = null;
            Map<String, GenreEntity> genres = null;

            List<FilmDto> filmDtos = ParserService.getFilmDtos(movieResponse, genres);

            assertNotNull(filmDtos);
            assertEquals(0, filmDtos.size());
        }

        @Test
        void whenPassValidMovie_thenReturnValid() {
            MovieDocsResponseDtoV13 movieResponse = MOVIE_DOCS_RESPONSE_1;
            Map<String, GenreEntity> genres = getGenres();

            List<FilmDto> filmDtos = ParserService.getFilmDtos(movieResponse, genres);

            assertNotNull(filmDtos);
            assertEquals(1, filmDtos.size());

            FilmDto expected = FilmDto.builder().name(FILM_NAME_1).genres(List.of(getGenre1())).release_year(YEAR).rating_kp(RATING_KP).rating_imdb(RATING_IMDB).poster_url(PREVIEW_URL).duration((int) DURATION).build();
            assertEquals(expected.toString(), filmDtos.get(0).toString());
        }

        @Test
        void whenPassValidMovieWithPrequelsAndSequels_thenReturnEmpty() {
            MovieDocsResponseDtoV13 movieResponse = MOVIE_DOCS_RESPONSE_3;
            Map<String, GenreEntity> genres = getGenres();

            List<FilmDto> filmDtos = ParserService.getFilmDtos(movieResponse, genres);

            assertNotNull(filmDtos);
            assertEquals(0, filmDtos.size());
        }

        @Test
        void whenPassTwoValidMovies_thenReturnTwoValidFilmDto() {
            MovieDocsResponseDtoV13 movieResponse = MOVIE_DOCS_RESPONSE_2;
            Map<String, GenreEntity> genres = getGenres();

            List<FilmDto> filmDtos = ParserService.getFilmDtos(movieResponse, genres);

            assertNotNull(filmDtos);
            assertEquals(2, filmDtos.size());

            FilmDto expected_1 = FilmDto.builder().name(FILM_NAME_1).genres(List.of(getGenre1())).release_year(YEAR).rating_kp(RATING_KP).rating_imdb(RATING_IMDB).poster_url(PREVIEW_URL).duration((int) DURATION).build();
            FilmDto expected_2 = FilmDto.builder().name(FILM_NAME_2).genres(List.of(getGenre2())).release_year(YEAR_2).rating_kp(RATING_KP).rating_imdb(RATING_IMDB).poster_url(PREVIEW_URL).duration((int) DURATION).build();
            assertEquals(expected_1.toString(), filmDtos.get(0).toString());
            assertEquals(expected_2.toString(), filmDtos.get(1).toString());
        }

        @Test
        void whenGenreForMovieNotFound_thenReturnSkipFilm() {
            MovieDocsResponseDtoV13 movieResponse = MOVIE_DOCS_RESPONSE_1;
            Map<String, GenreEntity> genres = Collections.emptyMap();

            List<FilmDto> filmDtos = ParserService.getFilmDtos(movieResponse, genres);

            assertNotNull(filmDtos);
            assertEquals(0, filmDtos.size());
        }


        @Test
        void validPageTotal() {
            MovieDocsResponseDtoV13 movieResponse = MOVIE_DOCS_RESPONSE_1;

            int pageTotal = ParserService.getPageTotal(movieResponse);

            assertEquals(10, pageTotal);
        }

        @Test
        void nullResponsePageTotal_thenReturnZero() {
            MovieDocsResponseDtoV13 movieResponse = null;

            int pageTotal = ParserService.getPageTotal(movieResponse);

            assertEquals(0, pageTotal);
        }

        @Test
        void whenMovieWithoutDuration_thenReturnSkipMovie() {
            MovieDocsResponseDtoV13 movieResponse = MOVIE_DOCS_RESPONSE_4;
            Map<String, GenreEntity> genres = Collections.emptyMap();

            List<FilmDto> filmDtos = ParserService.getFilmDtos(movieResponse, genres);

            assertNotNull(filmDtos);
            assertEquals(0, filmDtos.size());
        }

        @Test
        void validHttpHeaderAuth() {
            String key = "21klemwqs";

            HttpEntity<?> authHeaders = ParserService.getAuthHeaders(key);

            assertNotNull(authHeaders);
            HttpHeaders headers = authHeaders.getHeaders();
            assertEquals(1, headers.size());
            assertEquals(key, headers.get(ParserService.X_API_KEY).get(0));
        }

        @Test
        void invalidNullHttpHeaderAuth() {
            String key = null;
            Assertions.assertThrows(IllegalArgumentException.class, () -> ParserService.getAuthHeaders(key));
        }

        @Test
        void invalidEmptyHttpHeaderAuth() {
            String key = "";
            Assertions.assertThrows(IllegalArgumentException.class, () -> ParserService.getAuthHeaders(key));
        }
    }

    private Map<String, GenreEntity> getGenres() {
        return Map.of(GENRE_NAME_1, getGenre1(), GENRE_NAME_2, getGenre2());
    }

    @NotNull
    private static GenreEntity getGenre1() {
        GenreEntity genre = new GenreEntity();
        genre.setId(1L);
        genre.setName(GENRE_NAME_1);
        return genre;
    }

    @NotNull
    private static GenreEntity getGenre2() {
        GenreEntity genre = new GenreEntity();
        genre.setId(2L);
        genre.setName(GENRE_NAME_2);
        return genre;
    }



}
