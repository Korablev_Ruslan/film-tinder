package com.example.filmtinder.module.api.transformer;

import com.example.filmtinder.api.transformer.LobbyTransformer;
import com.example.filmtinder.openapi.model.Film;
import com.example.filmtinder.openapi.model.LobbyInfo;
import com.example.filmtinder.unit.lobby.LobbyEntity;
import com.example.filmtinder.unit.lobby.LobbyStatus;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

public class LobbyTransformerTest {

    LobbyTransformer underTest = new LobbyTransformer();

    @Nested
    public class RuslanLobbyTransformerTests {

        @Test
        void transformingEmptyEntityWithFilm() {
            LobbyInfo transformedEntity = underTest.transform(LobbyEntity.builder()
                            .status(LobbyStatus.FINISHED)
                            .chosenGenres(Set.of())
                            .joinedPersons(Set.of())
                            .build(),
                    new Film());

            LobbyInfo expected = new LobbyInfo();
            expected.setStatus(LobbyStatus.FINISHED.name());
            expected.joinedPersons(List.of());
            expected.chosenGenres(List.of());
            expected.setIsAvailableToStart(false);
            expected.setMatchedFilm(new Film());
            assertThat(transformedEntity)
                    .isEqualTo(expected);
        }

        @Test
        void testTransformingEmptyEntity() {
            LobbyInfo transformedEntity = underTest.transform(
                    LobbyEntity.builder()
                            .status(LobbyStatus.FINISHED)
                            .chosenGenres(Set.of())
                            .joinedPersons(Set.of())
                            .build()
            );
            LobbyInfo expected = new LobbyInfo();
            expected.setStatus(LobbyStatus.FINISHED.name());
            expected.joinedPersons(List.of());
            expected.chosenGenres(List.of());
            expected.setIsAvailableToStart(false);

            assertThat(transformedEntity)
                    .isEqualTo(expected);
        }

        @Test
        void testTransformingEntityWithoutStatus() {
            assertThatCode(() -> underTest.transform(
                    LobbyEntity.builder()
                            .chosenGenres(Set.of())
                            .joinedPersons(Set.of())
                            .build()
            )).isInstanceOf(NullPointerException.class);
        }

        @Test
        void testTransformingEntityGenresStatus() {
            assertThatCode(() -> underTest.transform(
                    LobbyEntity.builder()
                            .status(LobbyStatus.FINISHED)
                            .joinedPersons(Set.of())
                            .build()
            )).isInstanceOf(NullPointerException.class);
        }

    }

}