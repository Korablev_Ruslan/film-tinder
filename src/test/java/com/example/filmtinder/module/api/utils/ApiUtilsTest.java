package com.example.filmtinder.module.api.utils;

import com.example.filmtinder.api.utils.ApiUtils;
import com.example.filmtinder.db.film.FilmEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;

public class ApiUtilsTest {

    @Nested
    public class ArtyomApiUtilsTest {

        @Test
        void validFormatRating() {
            float rating = 1.66F;
            float actual = ApiUtils.formatRating(rating);
            Assertions.assertEquals(1.7F, actual);
        }

        @Test
        void validFormatRating_2() {
            float rating = 1.65F;
            float actual = ApiUtils.formatRating(rating);
            Assertions.assertEquals(1.6F, actual);
        }

        @Test
        void validFormatRating_3() {
            float rating = 1.6F;
            float actual = ApiUtils.formatRating(rating);
            Assertions.assertEquals(1.6F, actual);
        }

        @Test
        void validFormatRating_4() {
            float rating = 1.64F;
            float actual = ApiUtils.formatRating(rating);
            Assertions.assertEquals(1.6F, actual);
        }

        @Test
        void validFormatRating_5() {
            float rating = 1.666F;
            float actual = ApiUtils.formatRating(rating);
            Assertions.assertEquals(1.7F, actual);
        }

        @Test
        void roundRatingsForFilms_1() {
            FilmEntity film1 = FilmEntity.builder().rating_kp(1.66F).rating_imdb(1.45F).build();
            FilmEntity film2 = FilmEntity.builder().rating_kp(1.6F).rating_imdb(10.44F).build();
            List<FilmEntity> films = List.of(film1, film2);

            ApiUtils.roundRatings(films);

            Assertions.assertEquals(1.7F, film1.getRating_kp());
            Assertions.assertEquals(1.5F, film1.getRating_imdb());

            Assertions.assertEquals(1.6F, film2.getRating_kp());
            Assertions.assertEquals(10.4F, film2.getRating_imdb());
        }
    }
}
