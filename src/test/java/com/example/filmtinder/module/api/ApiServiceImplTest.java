package com.example.filmtinder.module.api;

import com.example.filmtinder.api.service.ApiServiceImpl;
import com.example.filmtinder.api.transformer.LobbyTransformer;
import com.example.filmtinder.db.film.FilmRepository;
import com.example.filmtinder.db.genre.GenreRepository;
import com.example.filmtinder.topOfFilmsFormer.TopOfFilmsFormerService;
import com.example.filmtinder.unit.lobby.LobbyEntity;
import com.example.filmtinder.unit.lobby.LobbyFilmEntity;
import com.example.filmtinder.unit.lobby.LobbyFilmRepository;
import com.example.filmtinder.unit.lobby.LobbyGenresRepository;
import com.example.filmtinder.unit.lobby.LobbyRepository;
import com.example.filmtinder.unit.lobby.LobbyStatus;
import com.example.filmtinder.user.context.UserContext;
import com.example.filmtinder.user.context.UserContextHolder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class ApiServiceImplTest {

    @Mock
    LobbyRepository lobbyRepository;
    @Mock
    UserContextHolder userContextHolder;
    @Spy
    LobbyTransformer lobbyTransformer;
    @Mock
    LobbyGenresRepository lobbyGenresRepository;
    @Mock
    LobbyFilmRepository lobbyFilmRepository;
    @Mock
    FilmRepository filmRepository;
    @Mock
    GenreRepository genreRepository;
    @Mock
    TopOfFilmsFormerService topOfFilmsFormerService;

    @InjectMocks
    ApiServiceImpl underTests;

    @Nested
    public class RuslanApiServiceImplTests {

        @ParameterizedTest(name = "[{index}] {0}")
        @MethodSource
        void likeFilm(Integer filmId) {
            String mockLobbyId = "mockLobbyId";
            lenient().when(lobbyFilmRepository.findLobbyFilmEntityByLobbyIdAndFilmId(mockLobbyId, (long) filmId))
                    .thenReturn(Optional.of(new LobbyFilmEntity("mock", mockLobbyId, (long) filmId, 1, 1)));

            underTests.likeFilm(mockLobbyId, filmId);
            Mockito.verify(lobbyFilmRepository).save(new LobbyFilmEntity("mock", mockLobbyId, (long) filmId, 2, 1));
        }

        @Test
        void likeFilmInNonExistingLobby() {
            String mockLobbyId = "mockLobbyId";
            int filmId = 10;
            lenient().when(lobbyFilmRepository.findLobbyFilmEntityByLobbyIdAndFilmId(mockLobbyId, (long) filmId))
                    .thenReturn(Optional.empty());

            assertThat(underTests.likeFilm(mockLobbyId, filmId)).isEmpty();
        }

        @Test
        void deleteExistingLike() {
            String mockLobbyId = "mockLobbyId";
            int filmId = 10;
            lenient().when(lobbyFilmRepository.findLobbyFilmEntityByLobbyIdAndFilmId(mockLobbyId, (long) filmId))
                    .thenReturn(Optional.of(new LobbyFilmEntity("mock", mockLobbyId, (long) filmId, 1, 1)));

            underTests.deleteLike(mockLobbyId, filmId);
            Mockito.verify(lobbyFilmRepository).save(new LobbyFilmEntity("mock", mockLobbyId, (long) filmId, 0, 1));
        }

        @Test
        void deleteNonExistingLike() {
            String mockLobbyId = "mockLobbyId";
            int filmId = 10;
            lenient().when(lobbyFilmRepository.findLobbyFilmEntityByLobbyIdAndFilmId(mockLobbyId, (long) filmId))
                    .thenReturn(Optional.empty());

            underTests.deleteLike(mockLobbyId, filmId);
            Mockito.verify(lobbyFilmRepository, Mockito.times(0)).save(any());
        }

        @Test
        void userFinishChoosingButNoOneFinished() {
            String mockLobbyId = "mockLobbyIdChosing";
            doReturn(
                    Optional.of(
                            LobbyEntity.builder()
                                    .lobbyId(mockLobbyId)
                                    .finishedCount(0)
                                    .joinedPersons(Set.of("1", "2", "3"))
                                    .status(LobbyStatus.FILMS_CHOOSING)
                                    .build()
                    )).when(lobbyRepository).findById(any());

            underTests.userFinishChoosing(mockLobbyId);
            Mockito.verify(lobbyRepository, Mockito.times(1)).save(any());
        }

        @Test
        void userFinishChoosingAndEveryoneFinished() {
            String mockLobbyId = "mockLobbyIdChosing";
            doReturn(
                    Optional.of(
                            LobbyEntity.builder()
                                    .lobbyId(mockLobbyId)
                                    .finishedCount(2)
                                    .joinedPersons(Set.of("1", "2", "3"))
                                    .status(LobbyStatus.FILMS_CHOOSING)
                                    .build()
                    )).when(lobbyRepository).findById(any());

            underTests.userFinishChoosing(mockLobbyId);
            Mockito.verify(lobbyRepository, Mockito.times(2)).save(any());
        }

        @Test
        void userFinishChoosingInNonExistingLobby() {
            String mockLobbyId = "mockLobbyIdChosingMiss";
            lenient().when(lobbyRepository.findById(mockLobbyId))
                    .thenReturn(Optional.empty());

            assertThat(underTests.userFinishChoosing(mockLobbyId)).isEmpty();
        }

        @Test
        void getFinishedNonExistingLobbyInfo() {
            String mockLobbyId = "mockLobbyIdChosingMiss";
            Mockito.when(lobbyRepository.findById(mockLobbyId))
                    .thenReturn(Optional.empty());

            assertThatCode(() -> underTests.getFinishedLobbyInfo(mockLobbyId)).hasMessageContaining("No value present")
                    .isInstanceOf(NoSuchElementException.class);
        }

        @Test
        void deleteLobbyInfo() {
            assertThatCode(() -> underTests.deleteLobbyInfo("123")).doesNotThrowAnyException();
        }

        @Test
        void restartLobbyNoRights() {
            String mockLobbyId = "mockLobbyIdRestart";

            doReturn(
                    Optional.of(
                            LobbyEntity.builder()
                                    .lobbyId(mockLobbyId)
                                    .finishedCount(2)
                                    .joinedPersons(Set.of("10", "2", "3"))
                                    .authorId("10")
                                    .status(LobbyStatus.FILMS_CHOOSING)
                                    .build()
                    )).when(lobbyRepository).findById(any());

            Mockito.when(userContextHolder.getContext()).thenReturn(new UserContext("2"));
            assertThatCode(() -> underTests.restartLobby(mockLobbyId)).isInstanceOf(RuntimeException.class);
        }

        @Test
        void restartLobbyWithRights() {
            String mockLobbyId = "mockLobbyIdRestart";
            doReturn(
                    Optional.of(
                            LobbyEntity.builder()
                                    .lobbyId(mockLobbyId)
                                    .finishedCount(2)
                                    .chosenGenres(Set.of())
                                    .joinedPersons(Set.of("10", "2", "3"))
                                    .authorId("10")
                                    .status(LobbyStatus.FILMS_CHOOSING)
                                    .build()
                    )).when(lobbyRepository).findById(any());

            Mockito.when(userContextHolder.getContext()).thenReturn(new UserContext("10"));
            assertThatCode(() -> underTests.restartLobby(mockLobbyId)).doesNotThrowAnyException();
            Mockito.verify(lobbyFilmRepository,
                    Mockito.times(3)).findLobbyFilmEntitiesByLobbyIdOrderByPositionAsc(mockLobbyId);
            Mockito.verify(lobbyFilmRepository).saveAll(any());
            Mockito.verify(lobbyGenresRepository).findLobbyGenresEntitiesByLobbyId(any());
            Mockito.verify(lobbyFilmRepository, Mockito.times(3)).findLobbyFilmEntitiesByLobbyIdOrderByPositionAsc(mockLobbyId);
        }

        @Test
        void getFilmsForLobbyInInvalidStatus() {
            String mockLobbyId = "mockLobbyIdGetFilms";
            doReturn(
                    Optional.of(
                            LobbyEntity.builder()
                                    .lobbyId(mockLobbyId)
                                    .finishedCount(2)
                                    .chosenGenres(Set.of())
                                    .joinedPersons(Set.of("10", "2", "3"))
                                    .authorId("10")
                                    .status(LobbyStatus.FINISHED)
                                    .build()
                    )).when(lobbyRepository).findById(any());
            assertThat(underTests.getFilmsForLobby(mockLobbyId)).isEmpty();
        }

        @Test
        void getFilmsForLobbyWithoutGenres() {
            String mockLobbyId = "mockLobbyIdGetFilms";
            doReturn(
                    Optional.of(
                            LobbyEntity.builder()
                                    .lobbyId(mockLobbyId)
                                    .finishedCount(2)
                                    .chosenGenres(Set.of())
                                    .joinedPersons(Set.of("10", "2", "3"))
                                    .authorId("10")
                                    .status(LobbyStatus.FINISHED)
                                    .build()
                    )).when(lobbyRepository).findById(any());
            assertThat(underTests.getFilmsForLobby(mockLobbyId)).isEmpty();
        }

        public static Stream<Arguments> likeFilm(){
            return Stream.iterate(1, item -> item + 1)
                    .limit(25)
                    .map(Arguments::of);
        }
    }
    @Nested
    public class ArtyomApiServiceTests {
        public static final int CODE_LENGTH = 6;

        @Test
        void validLobbyCodeGeneration() {
            String code = ApiServiceImpl.generateCode();

            Assertions.assertNotNull(code);
            Assertions.assertEquals(CODE_LENGTH, code.length());

            for (char c : code.toCharArray()) {
                Assertions.assertTrue(Character.isLetterOrDigit(c));
            }
        }
    }

}