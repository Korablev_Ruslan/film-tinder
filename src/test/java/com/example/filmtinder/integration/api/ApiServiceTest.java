package com.example.filmtinder.integration.api;

import com.example.filmtinder.api.service.ApiService;
import com.example.filmtinder.api.service.ApiServiceImpl;
import com.example.filmtinder.api.transformer.LobbyTransformer;
import com.example.filmtinder.db.genre.GenreEntity;
import com.example.filmtinder.db.genre.GenreRepository;
import com.example.filmtinder.module.utils.TestUtils;
import com.example.filmtinder.openapi.model.Film;
import com.example.filmtinder.openapi.model.LobbyInfo;
import com.example.filmtinder.parser.ParserService;
import com.example.filmtinder.unit.lobby.*;
import com.example.filmtinder.user.context.UserContext;
import com.example.filmtinder.user.context.UserContextHolder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Profile("cloud-db")
public class ApiServiceTest {

    @Autowired
    private ApiService apiService;

    @Autowired
    private ParserService parserService;
    @Autowired
    private UserContextHolder userContextHolder;
    @Autowired
    private LobbyRepository lobbyRepository;
    @Autowired
    private LobbyGenresRepository lobbyGenresRepository;
    @Autowired
    private LobbyFilmRepository lobbyFilmRepository;
    @Autowired
    private LobbyTransformer lobbyTransformer;
    @Autowired
    private GenreRepository genreRepository;

    @Nested
    public class PashaApiServiceTests {

        @Test
        public void whenNoJoinedPersons_thenNoLobby() {
            lobbyRepository.deleteAll();
            Optional<LobbyInfo> lobbyInfo = apiService.createLobby();
            Assertions.assertTrue(lobbyInfo.isEmpty());
            Assertions.assertTrue(lobbyRepository.findAll().isEmpty());
        }

        @Test
        public void whenSomeoneHasDeviceToken_thenHeMightCreateLobby() {
            final List<String> deviceTokens = List.of("A");

            LobbyInfo lobbyInfo = createLobbyWithCheck(deviceTokens.get(0));

            assertLobbyInfo(
                    lobbyInfo,
                    LobbyStatus.NOT_READY_TO_START,
                    deviceTokens
            );
        }

        @Test
        public void whenSomeoneHasInvalidDeviceToken_thenHeCantCreateLobby() {
            final List<String> deviceTokens = List.of("");

            Optional<LobbyInfo> lobbyInfoO = createLobbyWithoutCheck(deviceTokens.get(0));
            Assertions.assertTrue(lobbyInfoO.isEmpty());
        }

        @Test
        public void whenSomeoneHasJoinedToLobby_thenLobbyIsReadyToStart() {
            final List<String> deviceTokens = List.of("A", "B");

            LobbyInfo lobbyInfo = createLobbyWithCheck(deviceTokens.get(0));
            LobbyInfo lobbyInfoAfterOnePersonJoining = joinLobbyWithCheck(deviceTokens.get(1), lobbyInfo);

            assertLobbyInfo(
                    lobbyInfoAfterOnePersonJoining,
                    LobbyStatus.READY_TO_START,
                    deviceTokens
            );
        }

        @Test
        public void whenSomeoneTryToJoinToLobbyWithInvalidToken_thenHeWontBeJoinedToLobby_andLobbyWontReadyToStart() {
            final List<String> deviceTokens = new ArrayList<>(List.of("A", ""));
            deviceTokens.add(null);

            LobbyInfo lobbyInfo = createLobbyWithCheck(deviceTokens.get(0));
            Optional<LobbyInfo> lobbyInfoAfterOnePersonJoiningO = joinToLobbyWithoutCheck(deviceTokens.get(1),
                    lobbyInfo);
            Assertions.assertTrue(lobbyInfoAfterOnePersonJoiningO.isEmpty());

            LobbyInfo lobbyInfoAfterOnePersonJoining = getLobbyInfo(lobbyInfo.getLobbyId());

            assertThat(lobbyInfoAfterOnePersonJoining.getJoinedPersons(), hasSize(1));

            assertLobbyInfo(
                    lobbyInfoAfterOnePersonJoining,
                    LobbyStatus.NOT_READY_TO_START,
                    deviceTokens
            );
        }

        @Test
        public void whenSomeoneStartsLobbyWithValidToken_thenLobbyChangesItsStatusToGenresChoosing1() {
            final List<String> deviceTokens = List.of("A", "B");

            LobbyInfo lobbyInfo = createLobbyWithCheck(deviceTokens.get(0));
            LobbyInfo lobbyInfoAfterOnePersonJoining = joinLobbyWithCheck(deviceTokens.get(1), lobbyInfo);
            LobbyInfo startedLobbyInfo = startLobbyWithCheck(TestUtils.getRandomElement(deviceTokens),
                    lobbyInfoAfterOnePersonJoining);

            assertLobbyInfo(
                    startedLobbyInfo,
                    LobbyStatus.GENRES_CHOOSING,
                    deviceTokens
            );
        }

        @Test
        public void whenSomeoneStartsLobbyWithInvalidToken_thenNothingHappens() {
            final List<String> deviceTokens = List.of("A", "B", "");

            LobbyInfo lobbyInfo = createLobbyWithCheck(deviceTokens.get(0));
            LobbyInfo lobbyInfoAfterOnePersonJoining = joinLobbyWithCheck(deviceTokens.get(1), lobbyInfo);
            Optional<LobbyInfo> startedLobbyInfoO = startLobbyWithoutCheck(deviceTokens.get(2),
                    lobbyInfoAfterOnePersonJoining);

            Assertions.assertTrue(startedLobbyInfoO.isEmpty());

            LobbyInfo startedLobbyInfo = getLobbyInfo(lobbyInfo.getLobbyId());

            assertLobbyInfo(
                    startedLobbyInfo,
                    LobbyStatus.READY_TO_START,
                    deviceTokens
            );
        }

        @Test
        public void testGenresListIsPresentByDefault() {
            List<GenreEntity> genres = apiService.getGenres();
            Assertions.assertFalse(genres.isEmpty());
        }

        @Test
        public void whenSomeoneSaveGenresForLobby_thenNoCommonChosenGenres() {
            final List<String> deviceTokens = List.of("A", "B");

            LobbyInfo lobbyInfo = createLobbyWithCheck(deviceTokens.get(0));
            LobbyInfo lobbyInfoAfterOnePersonJoining = joinLobbyWithCheck(
                    deviceTokens.get(1),
                    lobbyInfo
            );
            LobbyInfo lobbyInfoAfterStarting = startLobbyWithCheck(
                    TestUtils.getRandomElement(deviceTokens),
                    lobbyInfoAfterOnePersonJoining
            );

            LobbyInfo lobbyInfoAfterSavingGenres = saveRandomGenres(
                    deviceTokens.get(0),
                    lobbyInfoAfterStarting
            );

            List<LobbyGenresEntity> lobbyGenresEntities = lobbyGenresRepository.findLobbyGenresEntitiesByLobbyId(
                    lobbyInfo.getLobbyId()
            );
            assertThat(lobbyGenresEntities, hasSize(1));

            assertLobbyInfo(
                    lobbyInfoAfterSavingGenres,
                    LobbyStatus.GENRES_CHOOSING,
                    deviceTokens
            );
        }

        @Test
        public void whenSomeoneSaveGenresForLobbyWithInvalidDeviceToken_thenHeWontChoiceAnyGenre_andThereIsNoCommonChosenGenres() {
            final List<String> deviceTokens = List.of("A", "B");

            LobbyInfo lobbyInfo = createLobbyWithCheck(deviceTokens.get(0));
            LobbyInfo lobbyInfoAfterOnePersonJoining = joinLobbyWithCheck(deviceTokens.get(1), lobbyInfo);
            LobbyInfo lobbyInfoAfterStarting = startLobbyWithCheck(TestUtils.getRandomElement(deviceTokens),
                    lobbyInfoAfterOnePersonJoining);

            LobbyInfo lobbyInfoAfterSavingGenres = saveRandomGenres(
                    "",
                    lobbyInfoAfterStarting
            );

            List<LobbyFilmEntity> lobbyFilmEntities =
                    lobbyFilmRepository.findLobbyFilmEntitiesByLobbyIdOrderByPositionAsc(lobbyInfo.getLobbyId());
            assertThat(lobbyFilmEntities, empty());

            assertLobbyInfo(
                    lobbyInfoAfterSavingGenres,
                    LobbyStatus.GENRES_CHOOSING,
                    deviceTokens
            );
        }

        @Test
        public void whenAllHaveSavedGenres_thenLobbyHasStatusFilmsChoosing() {
            final List<String> deviceTokens = List.of("A", "B");

            LobbyInfo lobbyInfo = createLobbyWithCheck(deviceTokens.get(0));
            LobbyInfo lobbyInfoAfterOnePersonJoining = joinLobbyWithCheck(deviceTokens.get(1), lobbyInfo);
            LobbyInfo lobbyInfoAfterStarting = startLobbyWithCheck(TestUtils.getRandomElement(deviceTokens),
                    lobbyInfoAfterOnePersonJoining);

            LobbyInfo lobbyInfoAfterSavingGenres1 = saveRandomGenres(
                    deviceTokens.get(0),
                    lobbyInfoAfterStarting
            );

            LobbyInfo lobbyInfoAfterSavingGenres2 = saveRandomGenres(
                    deviceTokens.get(1),
                    lobbyInfoAfterSavingGenres1
            );

            assertLobbyInfo(
                    lobbyInfoAfterSavingGenres2,
                    LobbyStatus.FILMS_CHOOSING,
                    deviceTokens,
                    lobbyInfoAfterSavingGenres2.getChosenGenres()
            );
        }
    }

    private void assertLobbyInfo(
            LobbyInfo lobbyInfo,
            LobbyStatus lobbyStatus,
            List<String> joinedPersons
    ) {
        assertLobbyInfo(
                lobbyInfo,
                lobbyStatus,
                joinedPersons,
                Collections.emptyList(),
                Collections.emptyList(),
                0
        );
    }

    private void assertLobbyInfo(
            LobbyInfo lobbyInfo,
            LobbyStatus lobbyStatus,
            List<String> joinedPersons,
            List<String> chosenGenreNames
    ) {
        assertLobbyInfo(
                lobbyInfo,
                lobbyStatus,
                joinedPersons,
                chosenGenreNames,
                Collections.emptyList(),
                0
        );
    }

    private void assertLobbyInfo(
            LobbyInfo lobbyInfo,
            LobbyStatus lobbyStatus,
            List<String> deviceTokens,
            List<String> chosenGenreIds,
            List<Film> films,
            int finishedCount
    ) {
        assertThat(lobbyInfo.getStatus(), equalTo(lobbyStatus.name()));
        assertThat(lobbyInfo.getFinishedCount(), equalTo(finishedCount));

        if (!chosenGenreIds.isEmpty()) {
            Assertions.assertFalse(lobbyInfo.getChosenGenres().isEmpty());
        }

        List<String> lobbyJoinedPersons = lobbyInfo.getJoinedPersons();
        if (deviceTokens.isEmpty()) {
            assertThat(lobbyJoinedPersons, empty());
        } else {
            List<String> nonEmptyDeviceTokens = deviceTokens
                    .stream()
                    .filter(deviceToken -> deviceToken != null && !deviceToken.isEmpty())
                    .toList();
            assertThat(lobbyJoinedPersons, is(nonEmptyDeviceTokens));
        }

        List<String> chosenGenreNames = chosenGenreIds;
//                .stream()
//                .map(genreId -> genreRepository.findById(Long.valueOf(genreId)).get().getName())
//                .toList();


        int fCount = (int) films.stream().filter(film -> film.getGenres().containsAll(chosenGenreNames)).count();
        Assertions.assertTrue(fCount > films.size() - 3);

        cleanup(lobbyInfo);
    }

    private Optional<LobbyInfo> startLobbyWithoutCheck(String deviceToken, LobbyInfo lobbyInfo) {
        setDeviceToken(deviceToken);
        Optional<LobbyInfo> startedLobbyInfoO = apiService.startLobby(lobbyInfo.getLobbyId());
        return startedLobbyInfoO;
    }

    private void setDeviceToken(String deviceToken) {
        userContextHolder.setContext(new UserContext(deviceToken));
    }

    public Optional<LobbyInfo> createLobbyWithoutCheck(String deviceToken) {
        setDeviceToken(deviceToken);
        return apiService.createLobby();
    }

    private LobbyInfo createLobbyWithCheck(String deviceToken) {
        setDeviceToken(deviceToken);
        Optional<LobbyInfo> lobbyInfoO = apiService.createLobby();
        Assertions.assertTrue(lobbyInfoO.isPresent());
        return lobbyInfoO.get();
    }

    private LobbyInfo getLobbyInfoWithCheck(String lobbyCode) {
        Optional<LobbyInfo> lobbyInfoO = apiService.getLobbyInfo(lobbyCode);
        Assertions.assertTrue(lobbyInfoO.isPresent());
        return lobbyInfoO.get();
    }

    private LobbyInfo joinLobbyWithCheck(String deviceToken, LobbyInfo lobbyInfo) {
        LobbyInfo curLobbyInfo = getLobbyInfoWithCheck(lobbyInfo.getLobbyId());
        int nJoinedPersons = curLobbyInfo.getJoinedPersons().size();
        setDeviceToken(deviceToken);
        Optional<LobbyInfo> lobbyInfoAfterJoiningO = apiService.joinLobby(lobbyInfo.getCode());
        Assertions.assertTrue(lobbyInfoAfterJoiningO.isPresent());
        LobbyInfo lobbyInfoAfterOnePersonJoining = lobbyInfoAfterJoiningO.get();
        assertThat(lobbyInfoAfterOnePersonJoining.getJoinedPersons(), hasSize(nJoinedPersons + 1));
        return lobbyInfoAfterJoiningO.get();
    }

    private Optional<LobbyInfo> joinToLobbyWithoutCheck(String deviceToken, LobbyInfo lobbyInfo) {
        setDeviceToken(deviceToken);
        return apiService.joinLobby(lobbyInfo.getCode());
    }

    private LobbyInfo startLobbyWithCheck(String deviceToken, LobbyInfo lobbyInfo) {
        setDeviceToken(deviceToken);
        Optional<LobbyInfo> startedLobbyInfoO = apiService.startLobby(lobbyInfo.getLobbyId());
        Assertions.assertTrue(startedLobbyInfoO.isPresent());
        return startedLobbyInfoO.get();
    }

    private List<GenreEntity> getRandomGenres(int nGenres) {
        List<GenreEntity> genres = new ArrayList<>(apiService.getGenres());
        if (genres.isEmpty()) {
            parserService.parseMePasha();
            try {
                Thread.sleep(15_000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            parserService.stopParsing();
            genres = new ArrayList<>(apiService.getGenres());
            if (genres.isEmpty()) {
                throw new RuntimeException("Something wrong with genres");
            }
        }
        Collections.shuffle(genres);
        return genres.subList(0, Math.min(nGenres, genres.size()));
    }

    private LobbyInfo saveGenresForLobbyWithCheck(LobbyInfo lobbyInfo, List<GenreEntity> chosenGenres) {
        List<String> genreIds = chosenGenres
                .stream()
                .map(GenreEntity::getName)
                .toList();
        Optional<LobbyInfo> lobbyInfoAfterSavingGenres = apiService.saveGenresForLobby(lobbyInfo.getLobbyId(),
                genreIds);
        Assertions.assertTrue(lobbyInfoAfterSavingGenres.isPresent());
        return lobbyInfoAfterSavingGenres.get();
    }

    private LobbyInfo saveRandomGenres(String deviceToken, LobbyInfo lobbyInfo) {
        setDeviceToken(deviceToken);
        List<GenreEntity> randomGenres = getRandomGenres(5);
        return saveGenresForLobbyWithCheck(lobbyInfo, randomGenres);
    }

    private LobbyInfo getLobbyInfo(String lobbyId) {
        Optional<LobbyEntity> lobbyEntityO = lobbyRepository.findById(lobbyId);
        Assertions.assertTrue(lobbyEntityO.isPresent());
        LobbyInfo lobbyInfo = lobbyTransformer.transform(lobbyEntityO.get());
        return lobbyInfo;
    }

    private void cleanup(LobbyInfo lobbyInfo) {
        String lobbyCode = lobbyInfo.getCode();

        LobbyEntity lobbyEntity = lobbyRepository.findLobbyEntityByCodeOrderByStartedDateDesc(lobbyCode).get();
        lobbyRepository.deleteById(lobbyEntity.getLobbyId());

        List<LobbyFilmEntity> lobbyFilmEntities =
                lobbyFilmRepository.findLobbyFilmEntitiesByLobbyIdOrderByPositionAsc(lobbyCode);
        lobbyFilmRepository.deleteAll(lobbyFilmEntities);

        List<LobbyGenresEntity> lobbyGenresEntities = lobbyGenresRepository.findLobbyGenresEntitiesByLobbyId(lobbyCode);
        lobbyGenresRepository.deleteAll(lobbyGenresEntities);
    }
}
