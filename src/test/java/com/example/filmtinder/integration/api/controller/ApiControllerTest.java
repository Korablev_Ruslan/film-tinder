package com.example.filmtinder.integration.api.controller;

import com.example.filmtinder.FilmTinderApplication;
import com.example.filmtinder.api.controller.ApiController;
import com.example.filmtinder.db.genre.GenreRepository;
import com.example.filmtinder.parser.ParserService;
import com.example.filmtinder.unit.lobby.LobbyRepository;
import com.example.filmtinder.user.context.UserContextHolder;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@WebMvcTest(ApiController.class)
@ComponentScan("com.example.filmtinder")
@AutoConfigureDataJpa
class ApiControllerTest {

    private final ObjectMapper objectMapper = new ObjectMapper();
    @Autowired
    MockMvc mvc;

    @Autowired
    LobbyRepository lobbyRepository;

    @Autowired
    ParserService parserService;
    @Autowired
    GenreRepository genreRepository;

    @BeforeEach
    public void parseFilms() throws InterruptedException {
        if (genreRepository.findAll().isEmpty()) {
            parserService.parseMePasha();
            Thread.sleep(30_000);
            parserService.stopParsing();
            if (genreRepository.findAll().isEmpty()) {
                throw new RuntimeException("No genres!");
            }
        }
    }

    @Test
    void invalidLobbyCreation() throws Exception {
        assertThat(mvc.perform(MockMvcRequestBuilders.post("/v1/lobbies").header("InvalidDeviceTokenValue", "1"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn()
                .getResponse().getStatus())
                .isEqualTo(400);
    }

    @Test
    void validLobbyCreation() throws Exception {
        assertThat(mvc.perform(MockMvcRequestBuilders.post("/v1/lobbies").header("X-Device-Token",
                        "validLobbyCreation"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn()
                .getResponse().getStatus())
                .isEqualTo(200);
    }

    @Test
    void getCreatedLobbyInfo() throws Exception {
        String lobbyId = objectMapper.readTree(mvc.perform(MockMvcRequestBuilders.post("/v1/lobbies")

                                        .header("X-Device-Token", "123")
                                )
                                .andDo(MockMvcResultHandlers.print())
                                .andReturn().getResponse().getContentAsString()
                )
                .get("lobbyId").asText();

        assertThat(objectMapper.readTree(
                        mvc.perform(MockMvcRequestBuilders.get("/v1/lobbies/" + lobbyId)
                                        .header("X-Device-Token", "123"))
                                .andDo(MockMvcResultHandlers.print())
                                .andReturn()
                                .getResponse()
                                .getContentAsString()
                )
                .get("lobbyId").asText()).isEqualTo(lobbyId);
    }

    @Test
    void getNonExistentLobbyInfo() throws Exception {
        assertThat(mvc.perform(MockMvcRequestBuilders.get("/v1/lobbies/" + "NonExistentLobby")
                        .header("X-Device-Token", "123"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn()
                .getResponse().getStatus()).isEqualTo(404);
    }

    @Test
    void validJoinLobby() throws Exception {
        String lobbyId = objectMapper.readTree(mvc.perform(MockMvcRequestBuilders.post("/v1/lobbies")
                                .header("X-Device-Token", "1234")
                        )
                        .andDo(MockMvcResultHandlers.print())

                        .andReturn().getResponse().getContentAsString())
                .get("lobbyId").asText();

        String token = objectMapper.readTree(mvc.perform(MockMvcRequestBuilders.get("/v1/lobbies/" + lobbyId)
                        .header("X-Device-Token", "1234"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn().getResponse().getContentAsString()).get("code").asText();

        JsonNode result = objectMapper.readTree(mvc.perform(MockMvcRequestBuilders.post("/v1/lobbies:join")
                        .param("token", token)
                        .header("X-Device-Token", "validJoinLobby"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn()
                .getResponse()
                .getContentAsString()
        );

        String joinedPersons = result.get("joinedPersons").toString();

        assertThat(joinedPersons).contains("1234", "validJoinLobby");
        assertThat(result.get("status").asText()).isEqualTo("READY_TO_START");
    }

    @Test
    void joinNonExistentLobby() throws Exception {
        assertThat(mvc.perform(MockMvcRequestBuilders.post("/v1/lobbies:join")
                        .param("token", "NonExistentLobbyToken")
                        .header("X-Device-Token", "12345"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn().getResponse()
                .getStatus()
        ).isEqualTo(404);
    }

    @Test
    void joinWithoutToken() throws Exception {
        assertThat(mvc.perform(MockMvcRequestBuilders.post("/v1/lobbies:join")
                        .header("X-Device-Token", "12345"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn()
                .getResponse()
                .getStatus()
        ).isEqualTo(400);
    }

    @Test
    void joinWithoutDeviceToken() throws Exception {
        assertThat(mvc.perform(MockMvcRequestBuilders.post("/v1/lobbies:join"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn()
                .getResponse()
                .getStatus()
        ).isEqualTo(400);
    }

    @Test
    void startLobby() throws Exception {
        String lobbyId = getLobbyId();
        String token = objectMapper.readTree(mvc.perform(MockMvcRequestBuilders.get("/v1/lobbies/" + lobbyId)
                        .header("X-Device-Token", "saveGenresForLobby"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn().getResponse().getContentAsString()).get("code").asText();

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/v1/lobbies:join")
                        .param("token", token)
                        .header("X-Device-Token", "saveGenresForLobby_2"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        JsonNode resultJson = objectMapper.readTree(result.getResponse().getContentAsString());
        assertThat(result.getResponse().getStatus()).isEqualTo(200);
        assertThat(resultJson.get("status").asText()).isEqualTo("READY_TO_START");

        assertThat(mvc.perform(MockMvcRequestBuilders.post("/v1/lobbies/" + lobbyId + "/start")
                        .header("X-Device-Token", "saveGenresForLobby")
                )
                .andDo(MockMvcResultHandlers.print())
                .andReturn()
                .getResponse()
                .getStatus()).isEqualTo(200);

        List<String> genres = objectMapper.readValue(
                mvc.perform(MockMvcRequestBuilders.get("/v1/genres")
                                .header("X-Device-Token", "startLobbyTest")
                        )
                        .andDo(MockMvcResultHandlers.print())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(StandardCharsets.UTF_8),
                new TypeReference<>() {
                }
        );
        assertThat(mvc.perform(MockMvcRequestBuilders.post("/v1/lobbies/" + lobbyId + "/sessions")
                        .param("ids", genres.get(0))
                        .header("X-Device-Token", "saveGenresForLobby_2"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn().getResponse().getStatus()).isEqualTo(200);
        assertThat(mvc.perform(MockMvcRequestBuilders.post("/v1/lobbies/" + lobbyId + "/sessions")
                        .param("ids", genres.get(0))
                        .header("X-Device-Token", "saveGenresForLobby"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn().getResponse().getStatus()).isEqualTo(200);

        mvc.perform(MockMvcRequestBuilders.get("/v1/lobbies/" + lobbyId + "/films")
                        .header("X-Device-Token", "saveGenresForLobby"))
                .andDo(MockMvcResultHandlers.print());


    }

    @Test
    void getGenres() throws Exception {
        List<String> genres = objectMapper.readValue(
                mvc.perform(MockMvcRequestBuilders.get("/v1/genres")
                                .header("X-Device-Token", "startLobbyTest")
                        )
                        .andDo(MockMvcResultHandlers.print())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(),
                new TypeReference<>() {
                }
        );
        assertThat(genres).isNotEmpty();
    }

    @Test
    void saveGenresForLobbyInvalidStatus() throws Exception {
        String lobbyId = objectMapper.readTree(mvc.perform(MockMvcRequestBuilders.post("/v1/lobbies")
                        .header("X-Device-Token", "startLobbyTest")
                )
                .andDo(MockMvcResultHandlers.print())
                .andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8)).get("lobbyId").asText();


        List<String> genres = objectMapper.readValue(
                mvc.perform(MockMvcRequestBuilders.get("/v1/genres")
                                .header("X-Device-Token", "startLobbyTest")
                        )
                        .andDo(MockMvcResultHandlers.print())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(StandardCharsets.UTF_8),
                new TypeReference<>() {
                }
        );

        assertThat(mvc.perform(MockMvcRequestBuilders.post("/v1/lobbies/" + lobbyId + "/sessions")
                        .param("ids", genres.get(0))
                        .header("X-Device-Token", "saveGenresForLobby"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn()
                .getResponse()
                .getStatus()
        ).isEqualTo(404);
    }

    private String getLobbyId() throws Exception {
        return objectMapper.readTree(mvc.perform(MockMvcRequestBuilders.post("/v1/lobbies")
                        .header("X-Device-Token", "saveGenresForLobby")
                )
                .andDo(MockMvcResultHandlers.print())
                .andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8)).get("lobbyId").asText();
    }

    @Test
    void saveInvalidNumberOfGenres() throws Exception {
        String lobbyId = getLobbyId();


        assertThat(mvc.perform(MockMvcRequestBuilders.post("/v1/lobbies/" + lobbyId + "/sessions")
                        .header("X-Device-Token", "saveGenresForLobby"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn()
                .getResponse()
                .getStatus()
        ).isEqualTo(400);
    }
}