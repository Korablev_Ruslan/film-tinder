package com.example.filmtinder.integration.db.genre;

import com.example.filmtinder.db.genre.GenreDto;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Random;

public class GenreTestUtils {

    private static final Random random = new Random();

    public static GenreDto generateRandomGenre() {
        return GenreDto.builder()
                .name(generateRandomString())
                .build();
    }

    public static String generateRandomString() {
        int length = 10;
        boolean useLetters = true;
        boolean useNumbers = false;
        return RandomStringUtils.random(length, useLetters, useNumbers);
    }
}
