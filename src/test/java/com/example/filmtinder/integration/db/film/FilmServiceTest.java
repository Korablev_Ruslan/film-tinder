package com.example.filmtinder.integration.db.film;

import com.example.filmtinder.db.film.FilmDto;
import com.example.filmtinder.db.film.FilmEntity;
import com.example.filmtinder.db.film.FilmService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Profile("cloud-db")
public class FilmServiceTest {

    @Autowired
    private FilmService filmService;

    @Nested
    public class PashaFilmServiceTests {

        @Test
        public void whenSaveValidFilm_thenNoErrors() {
            FilmDto film = FilmTestUtils.generateRandomFilm();
            createAndCheckOk(film);
        }

        @Test
        public void whenSaveFilmWithInvalidName_thenNoErrors() {
            FilmDto film = FilmTestUtils.generateRandomFilm();
            film.setName("");
            createAndCheckOk(film);
        }

        @Test
        public void whenSaveFilmWithInvalidDescription_thenNoErrors() {
            FilmDto film = FilmTestUtils.generateRandomFilm();
            film.setDescription("");
            createAndCheckOk(film);
        }

        @Test
        public void whenSaveFilmWithInvalidPosterUrl_thenNoErrors() {
            FilmDto film = FilmTestUtils.generateRandomFilm();
            film.setPoster_url("myurl");
            createAndCheckOk(film);
        }

        @Test
        public void whenSaveFilmWithInvalidRatingKp_thenNoErrors() {
            FilmDto film = FilmTestUtils.generateRandomFilm();
            film.setRating_kp(-15.0f);
            createAndCheckOk(film);
        }

        @Test
        public void whenSaveFilmWithInvalidRatingImdb_thenNoErrors() {
            FilmDto film = FilmTestUtils.generateRandomFilm();
            film.setRating_imdb(-30.0f);
            createAndCheckOk(film);
        }

        @Test
        public void whenSaveFilmWithInvalidReleaseYear_thenNoErrors() {
            FilmDto film = FilmTestUtils.generateRandomFilm();
            film.setRelease_year(3000);
            createAndCheckOk(film);
        }

        @Test
        public void whenSaveFilmWithInvalidDuration_thenNoErrors() {
            FilmDto film = FilmTestUtils.generateRandomFilm();
            film.setDuration(-15);
            createAndCheckOk(film);
        }
    }

    private void createAndCheckOk(FilmDto filmDto) {
        Optional<FilmEntity> createdFilmO = filmService.create(filmDto);
        Assertions.assertTrue(createdFilmO.isPresent());
        FilmEntity createdFilm = createdFilmO.get();
        Assertions.assertTrue(filmEqualsFilmDto(createdFilm, filmDto));
        filmService.delete(createdFilm.getId());
    }

    private boolean filmEqualsFilmDto(FilmEntity film, FilmDto filmDto) {
        return film.getName().equals(filmDto.getName())
                && film.getDescription().equals(filmDto.getDescription())
                && film.getPoster_url().equals(filmDto.getPoster_url())
                && film.getRating_kp().equals(filmDto.getRating_kp())
                && film.getRating_imdb().equals(filmDto.getRating_imdb())
                && film.getRelease_year().equals(filmDto.getRelease_year())
                && film.getDuration().equals(filmDto.getDuration());
    }

}
