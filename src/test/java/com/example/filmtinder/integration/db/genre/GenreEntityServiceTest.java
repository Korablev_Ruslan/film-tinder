package com.example.filmtinder.integration.db.genre;

import com.example.filmtinder.db.film.FilmRepository;
import com.example.filmtinder.db.genre.GenreDto;
import com.example.filmtinder.db.genre.GenreEntity;
import com.example.filmtinder.db.genre.GenreService;
import com.example.filmtinder.utils.Cache;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Profile("cloud-db")
public class GenreEntityServiceTest {

    @Autowired
    private FilmRepository filmRepository;
    @Autowired
    private GenreService genreService;

    @Nested
    public class PashaGenreServiceTestsEntity {
        @Test
        public void whenSaveGenreWithValidName_thenNoErrors() {
            GenreDto genreDto = GenreDto
                    .builder()
                    .name(Cache.VALID_GENRE_NAMES.get(0))
                    .build();
            createAndCheckOk(genreDto);
        }

        @Test
        public void whenSaveGenreWithInvalidName_thenNoErrors() {
            GenreDto genreDto = GenreTestUtils.generateRandomGenre();
            createAndCheckOk(genreDto);
        }
    }

    private void createAndCheckOk(GenreDto genreDto) {
        Optional<GenreEntity> createdGenreO = genreService.create(genreDto);
        Assertions.assertTrue(createdGenreO.isPresent());
        GenreEntity createdGenre = createdGenreO.get();
        Assertions.assertTrue(genreEqualsGenreDto(createdGenre, genreDto));
        genreService.delete(createdGenre.getId());
    }

    private boolean genreEqualsGenreDto(GenreEntity genre, GenreDto genreDto) {
        return genre.getName().equals(genreDto.getName());
    }
}
