package com.example.filmtinder.integration.db.film;

import com.example.filmtinder.db.film.FilmDto;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Random;

public class FilmTestUtils {

    private static final Random random = new Random();

    public static FilmDto generateRandomFilm() {
        return FilmDto.builder()
                .name(generateRandomString())
                .description(generateRandomString())
                .poster_url(generateRandomString())
                .rating_kp(generateRandomRating())
                .rating_imdb(generateRandomRating())
                .release_year(generateRandomYear())
                .duration(generateRandomDuration())
                .build();
    }

    public static String generateRandomString() {
        int length = 10;
        boolean useLetters = true;
        boolean useNumbers = false;
        return RandomStringUtils.random(length, useLetters, useNumbers);
    }

    public static Float generateRandomRating() {
        return random.nextFloat(1, 10);
    }

    public static Integer generateRandomYear() {
        return random.nextInt(1900, 2023);
    }

    public static Integer generateRandomDuration() {
        return random.nextInt(60, 180);
    }
}
