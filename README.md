# MovieMate: Watch Together

**MovieMate: Watch Together** - приложение, созданное для генерации релевантных
подборок фильмов по жанрам для двоих и поиска первого обоюдно понравившегося фильма.
**MovieMate: Watch Together** позволяет двум людям, находящимся рядом,
выбрать кино для совместного досуга.

## Стек
- Spring:
  - Boot
  - Web
  - Data JPA
- Docker Compose
- Lombok
- PostgreSQL
- Liquibase
- Java 17
- JUnit

## Тесты

### Как использовать JaCoCo

1) убедитесь, что в вашем Test-классе использованы тестовые аннотации из `orj.junit`,
   типа `import org.junit.Test`, аннотации из других пакетов не сработают для JaCoCo!
   (вроде бы, проверяйте сами, у меня уже сил нет)
2) запустите

```commandline
mvn compile test
```

3) если все ок, то в `target/surefire-reports/index.html` ИЛИ `target/site/jacoco/index.html`
   будет сгенерирован отчет JaCoCo о тестовом покрытии.

### Модульное тестирование

Для однообразности кода рекомендуется следующее:

1) модульные тесты хранить в пакете [test/.../filmtinder/module](src/test/java/com/example/filmtinder/module)
2) Test-классы раскладывать по папкам в соответствие с иерархией в [src/java](src/main/java/com/example/filmtinder)
3) Свои проверки внутри Test-класса агрегировать при помощи
   аннотации `@Nested` ([пример](src/test/java/com/example/filmtinder/module/db/film/FilmServiceTest.java))
4) Когда все проверки будут написаны, следует сагрегировать их при помощи аннотации `@Suite` в
   пакете `test/.../filmtinder/suites` ([пример](src/test/java/com/example/filmtinder/suites/PashaTestSuite.java))

Тест-сьюты помещаются на уровень выше проводимого типа тестирования для дальнейших нужд
(там будут агрегироваться именные интеграционные тесты и другие).

### Интеграционное тестирование

#### TestContainers

В проекте используется TestContainers. Небольшой гайд по написанию интеграционного теста, взаимодействующего с БД:

1) переходим в [src/test/.../integration](src/test/java/com/example/filmtinder/integration)
2) создаем свой пакет, в котором что-то будем тестировать, если он еще не создан
3) создаем свой класс, наследуемый
   от [PostgreSQLContainerTest](src/test/java/com/example/filmtinder/integration/containers/PostgreSQLContainerTest.java)
4) создаем внутри этого класса `@Nested` класс со своим именем (например,
   [`PashaTestContainersTest`](src/test/java/com/example/filmtinder/integration/PostgreSQLContainerSampleTest.java))
5) в нем пишем что хотим, в основном все же тесты
6) переходим в свой именованный `TestSuite` из пакета [suites](src/test/java/com/example/filmtinder/suites),
там находим уже готовый класс со схемой - `<MemberName>IntegrationTestSuite`, в него селектим свои тесты
7) переходим в [GeneralTestSuite#IntegrationTestSuite](src/test/java/com/example/filmtinder/suites/GeneralTestSuite.java)
8) запускаем и наслаждаемся жизнью

Также допустимо создать контейнер для чего-то другого, отличного от `PostgreSQL`. Вот как:

1) переходим в [IntegrationUtils](src/test/java/com/example/filmtinder/integration/utils/IntegrationUtils.java)
2) добавляем по образу и подобию свой `static method`
3) переходим в пакет [containers](src/test/java/com/example/filmtinder/integration/containers)
4) создаем свой тестовый `abstract class` по схеме - `<Smth>ContainerTest`,
   пример можно посмотреть в рядом
   лежащем [PostgreSQLContainerTest](src/test/java/com/example/filmtinder/integration/containers/PostgreSQLContainerTest.java)
5) наследуемся от него там, где надо
6) запускаем и наслаждаемся жизнью
